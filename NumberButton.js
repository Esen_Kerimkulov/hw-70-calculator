import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

export default class NumberButton extends React.Component {
    render() {
        const {value, handleOnPress} = this.props;
        return (
                <TouchableOpacity style={styles.container} onPress={() => handleOnPress(value)}>
                    <Text style={styles.text}>{value}</Text>
                </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        margin: 1,
        backgroundColor: '#999966',
        borderRadius: 11,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: 'black',
        fontSize: 26
    }
});